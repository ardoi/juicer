#Analysis program for microscope images
##Features:
1. Open most microscope image formats
2. Analyze line- and framescan images
3. Perform release event analysis with denoising algorithm
4. Clustering of similar release events
5. Much more

Further details in the [user guide](doc/user_guide.pdf)

##Analysis
![analysis_image](http://i.imgur.com/cLqBihXl.png)
##Denoising
![Denoising image](http://i.imgur.com/zSfZrG0l.png)
##Clustering and event selection
![Denoising image](http://i.imgur.com/kUcV0vBl.png)
