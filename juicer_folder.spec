a = Analysis(['run_qt.py'],
             #pathex=['/Users/ardo/code/ardoi/juicer'],
             hiddenimports=['sklearn.utils.sparsetools._graph_validation',
                'PyQt5.Qt', 
                'sklearn.utils.lgamma',
                'sklearn.neighbors.typedefs',
                'sklearn.utils.weight_vector',
                '_sysconfigdata'],
             excludes=['matplotlib'],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='Juicer',
          debug=False,
          strip=None,
          upx=True,
          console=True )
bftools = Tree('lib/bftools',prefix='bftools')
coll = COLLECT(exe,
               a.binaries,
               bftools,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='resources')
