# -*- mode: python -*-
a = Analysis(['run_qt.py'],
             pathex=['/Users/ardo/code/ardoi/juicer'],
             hiddenimports=['scipy.special._ufuncs_cxx', '_sysconfigdata'],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='run_qt',
          debug=False,
          strip=None,
          upx=True,
          console=False )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='run_qt')
app = BUNDLE(coll,
             name='run_qt.app',
             icon=None)
