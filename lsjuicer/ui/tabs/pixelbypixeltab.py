import datetime
import numpy as np
from PyQt5 import QtGui as QG
from PyQt5 import QtWidgets as QW


from lsjuicer.data.analysis.pixbypixanalyzer import PixelByPixelAnalyzer
from lsjuicer.data.imagedata import ImageDataMaker, ImageDataLineScan, ImageDataFrameScan, make_selection
from lsjuicer.inout.db import sqla as sa
from lsjuicer.static.constants import ImageSelectionTypeNames as ISTN
from lsjuicer.ui.widgets.basicpixmapplotwidget import BasicPixmapPlotWidget
from lsjuicer.ui.widgets.clusterwidget import ClusterDialog
from lsjuicer.ui.widgets.pixeltracesplotwidget import PixelTracesPlotWidget
from lsjuicer.util.helpers import timeIt
from lsjuicer.ui.dialogs.fitdialog import FitDialog
import lsjuicer.data.analysis.transient_find as tf
from lsjuicer.ui.dialogs.saveeventsdialog import SaveEventsDialog


class PixelByPixelTab(QW.QTabWidget):

    def settings_text(self):
        out_image = "<strong>Image:</strong> <br>Width: %i<br>Height: %i<br>Pixels in frame: %i<br>Frames: %i"\
            % (self.imagedata.x_points, self.imagedata.y_points,
               self.imagedata.x_points * self.imagedata.y_points, self.imagedata.frames)

        if self.coords:
            out_selection = "<strong>Selection:</strong><br>Top left: x=%i y=%i<br>Width: %i<br>Height: %i<br>Pixels: " \
                            "%i<br/>Frames: %i" % (self.coords.left, self.coords.top, self.coords.width,
                                                   self.coords.height, self.coords.width *
                                                   self.coords.height,
                                                   self.coords.acquisitions)
        else:
            out_selection = ""
        out_settings = "<strong>Fit settings:</strong> <br>Traces to fit: %i"\
            % (self.analyzer.trace_count)
        return "<br><br>".join((out_image, out_selection, out_settings))

    @property
    def status(self):
        return ""

    def makefits(self):
        fit_dialog = FitDialog(self.analyzer, parent=self)
        fit_dialog.progress_map_update.connect(self.set_progress_map_data)
        self.threader = fit_dialog.d
        res = fit_dialog.exec_()
        if res:
            self.get_res()
        # print 'input=',params
        # self.threader.do(params)
        # print "total time for pool size %i and %i fits: %i seconds, %.2f seconds per fit"%(pool_size, self.trace_count, time.time()-t0, (time.time()-t0)/(self.trace_count))
        # return "<br>".join(out)

    def set_progress_map_data(self, data):
        self.progress_map_data = data
        self.plot_widget.set_data(data)

    @property
    def dx(self):
        if isinstance(self.imagedata, ImageDataLineScan):
            return 0
        else:
            return self.padding_spinbox.value()

    @property
    def dy(self):
        return self.padding_spinbox.value()

    def set_info_text(self):
        text = self.settings_text() + "<br><br>" + self.status
        self.info_widget.setHtml(text)

    def __init__(self, imagedata, selections, analysis, parent=None):
        super(PixelByPixelTab, self).__init__(parent)
        self.parent = parent
        self.coords = make_selection(imagedata)
        try:
            roi = selections[ISTN.ROI][0].graphic_item.rect()
            print "**ROI IS", roi

            self.coords.top = int(roi.top())
            self.coords.bottom = int(roi.bottom())
            if isinstance(imagedata, ImageDataFrameScan):
                # only set the right,left bounds for framescans
                # for linescans the right,left bounds are
                # actually start and end frames
                self.coords.right = int(roi.right())
                self.coords.left = int(roi.left())
            elif isinstance(imagedata, ImageDataLineScan):
                self.coords.start = int(roi.left())
                self.coords.end = int(roi.right())
        except IndexError:
            pass
        # send to pixelbypixelanalyzer
        try:
            if isinstance(imagedata, ImageDataFrameScan):
                time_range = selections[ISTN.TIMERANGE]
                self.coords.start = time_range['start']
                self.coords.end = time_range['end']
        except:
            # no time selection made
            pass

        self.fit = False
        self.analysis = analysis
        self.imagedata = imagedata

        self.analyzer = PixelByPixelAnalyzer(imagedata, analysis, self.coords)

        layout = QW.QVBoxLayout()
        self.setLayout(layout)
        settings_box = QW.QGroupBox('Fit settings')
        settings_layout = QW.QVBoxLayout()
        settings_box.setLayout(settings_layout)
        layout.addWidget(settings_box)

        padding_layout = QW.QHBoxLayout()
        padding_label = QW.QLabel("Element padding")
        padding_layout.addWidget(padding_label)
        padding_spinbox = QW.QSpinBox(self)
        padding_layout.addWidget(padding_spinbox)
        padding_spinbox.setMinimum(0)
        padding_spinbox.setValue(1)
        self.padding_spinbox = padding_spinbox
        padding_spinbox.setMaximum(20)
        settings_layout.addLayout(padding_layout)
        padding_spinbox.valueChanged.connect(self.set_info_text)

        limit_layout = QW.QHBoxLayout()
        limit_checkbox = QW.QCheckBox("Ignore transients less than x stds")
        self.limit_checkbox = limit_checkbox
        limit_layout.addWidget(limit_checkbox)
        limit_spinbox = QW.QSpinBox(self)
        self.limit_spinbox = limit_spinbox
        limit_layout.addWidget(limit_spinbox)
        limit_checkbox.toggled.connect(limit_spinbox.setEnabled)
        limit_spinbox.setEnabled(False)
        limit_spinbox.setMinimum(1)
        limit_spinbox.setMaximum(10)
        settings_layout.addLayout(limit_layout)

        info_widget = QW.QTextEdit(self)
        info_widget.setReadOnly(True)
        self.info_widget = info_widget
        self.set_info_text()
        settings_layout.addWidget(info_widget)
        fit_pb = QW.QPushButton("Do fit")
        # previous_pb = QG.QPushButton("Show previous")
        settings_layout.addWidget(fit_pb)
        # settings_layout.addWidget(previous_pb)
        fit_pb.clicked.connect(self.makefits)
        # previous_pb.clicked.connect(self.get_res)
        self.plot_widget = BasicPixmapPlotWidget(self)
        self.pixel_trace_widget = PixelTracesPlotWidget(self.plot_widget.scene,
                                                        self, parent=self)
        plots_layout = QW.QHBoxLayout()
        plots_layout.addWidget(self.plot_widget)
        plots_layout.addWidget(self.pixel_trace_widget)
        layout.addLayout(plots_layout)
        layout.setStretch(0, 1)
        layout.setStretch(1, 2)
        self.load_analysis()

    def load_analysis(self):
        if self.analysis.fitregions:
            self.get_res()

    def get_res(self):
        results = {}
        if self.analysis.fitregions:
            self.fit_result = self.analysis.fitregions[0].results[0]
        else:
            self.analyzer.make_result()
            #session = dbmaster.get_session()
            self.fit_result = self.analyzer.fit_result
        fitted_pixels = self.fit_result.pixels
        # print "get res", self.fit_result.region.width,
        # self.fit_result.region.height
        results['width'] = self.fit_result.region.width - \
            2 * self.fit_result.fit_settings['padding']
        results['height'] = self.fit_result.region.height - \
            2 * self.fit_result.fit_settings['padding']
        # FIXME
        #results['frames'] = self.fit_result.region.analysis.imagefile.image_frames
        results['frames'] = self.analyzer.coords.acquisitions
        results['dx'] = self.fit_result.fit_settings['padding']
        results['dy'] = self.fit_result.fit_settings['padding']
        results['x0'] = self.fit_result.region.x0
        results['y0'] = self.fit_result.region.y0
        results['fits'] = fitted_pixels
        self.res = results
        param_combo = QW.QComboBox()
        events = tf.data_events(self.res)
        # convert numpy.int64 to python int so that sqlalchemy can
        # understand it
        max_n = int(events.max())
        # max_index = [int(el) for el in
        #        numpy.unravel_index(events.argmax(), events.shape)]
        #+1 because in results the indices are skewed by one
        # print self.fit_result.id, max_index
        #sample_pixel = self.fit_result.get_fitted_pixel(max_index[1], max_index[0])
        # FIXME Bad bad bad
        parameters = ['A', 'm2', 'tau2', 'd', 'd2', 's']
        # for key in sample_pixel.pixel_events[0].parameters:
        for key in parameters:
            param_combo.addItem(key)
        param_combo.addItem('Events')
        param_combo.currentIndexChanged[str].connect(self.param_combo_changed)

        n_combo = QW.QComboBox()
        for i in range(max_n):
            n_combo.addItem("%i" % i)
        n_combo.currentIndexChanged.connect(self.n_combo_changed)

        hlayout = QW.QHBoxLayout()
        hlayout.addWidget(param_combo)
        hlayout.addWidget(n_combo)
        self.layout().addLayout(hlayout)
        self.t_number = 0
        self.param = "A"
        save_events_pb = QW.QPushButton(QG.QIcon(':/report_disk.png'), "Save all events")
        save_events_pb.clicked.connect(self.save_all_events)
        pb_do_clustering = QW.QPushButton("Do clustering")
        pb_do_clustering.setCheckable(True)
        pb_do_clustering.clicked.connect(self.do_clustering)
        pb_make_new_stack = QW.QPushButton("New stack from fit")
        pb_make_new_stack.clicked.connect(self.make_new_stack)
        hlayout.addWidget(save_events_pb)
        hlayout.addWidget(pb_do_clustering)
        hlayout.addWidget(pb_make_new_stack)
        self.show_res()

    def save_all_events(self):
        dialog = SaveEventsDialog(parent=self)
        if dialog.exec_():
            fullfname = str(dialog.save_file_name)
            comment = str(dialog.comment)
            f = open(fullfname, 'w')
            f.write("#Saved on: %s\n" % str(datetime.datetime.now()))
            f.write("#Comment: %s\n" % comment)
            f.write("#Image: %s\n" % self.imagedata.name)
            f.write("#Timestep unit: frame count (multiply temporal parameters with frame acquisition time to get values in time units)\n")
            f.write("#Frame acquisition time: {:.5f} ms\n".format(self.imagedata.delta_time))
            f.write("#Selection\n")
            f.write("##Position relative to image: left:{}\ttop:{}\n".format(self.coords.left, self.coords.top))
            f.write("##Dimensions: width:{}\theight:{}\tpixels:{}\tframes:{}\n".format(self.coords.width, self.coords.height, self.coords.width * self.coords.height, self.coords.acquisitions))
            pixels = self.fit_result.pixels
            parameters = ['A', 'd', 'tau2', 'm2', 'd2']
            cnames = ['x_loc', 'y_loc', 'pixel_event_number'] + parameters + ['FDHM']
            header = "#\n#Column names follow ->\n"
            header += ", ".join(cnames)
            header += "\n"
            f.write(header)
            rows = []
            for pixel in pixels:
                for i, event in enumerate(pixel.pixel_events):
                    row_data = [str(el) for el in [pixel.x, pixel.y, i]]
                    sol = event.parameters
                    params = ["{:.5f}".format(sol.get(p)) for p in parameters]
                    row_data.extend(params)
                    fdhm = sol['d2'] + sol['tau2'] * np.log(2.0) + sol['d'] / 2. * np.log((np.exp(2.0) + 1) / 2.)
                    row_data.append("{:.5f}".format(fdhm))
                    rows.append(", ".join(row_data) + "\n")
            for row in rows:
                f.write(row)
            f.close()
            QW.QMessageBox.information(
                self, 'Done', "%i rows saved to %s" % (len(rows), fullfname))
            # self.save_label.setText("Saved")

    def make_new_stack(self):
        #session = dbmaster.get_session()
        from lsjuicer.ui.tabs.imagetabs import AnalysisImageTab
        synthetic_image = sa.PixelFittedSyntheticImage(self.fit_result)
        next_tab = AnalysisImageTab(parent=self, analysis=self.analysis)
        im_data = ImageDataMaker.from_db_image(synthetic_image)
        next_tab.showData(im_data)
        next_tab.setAW(self.parent)
        next_icon = QG.QIcon(':/chart_curve.png')
        self.parent.add_tab(next_tab, next_icon, "Fitted data")

    def param_combo_changed(self, param):
        self.param = str(param)
        self.show_res()

    def n_combo_changed(self, t_number):
        self.t_number = t_number
        self.show_res()

    @timeIt
    def show_res(self):
        if self.param == "Events":
            data = tf.data_events(self.res)
        else:
            data = tf.make_data_by_size(self.res, self.param, self.t_number)
        self.plot_widget.set_data(data)

    def do_clustering(self, checked):
        dialog = ClusterDialog(self.analysis, self)
        dialog.show()
        # self.pixel_trace_widget.setVisible(checked)
