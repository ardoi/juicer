from PyQt5 import QtCore as QC

from PyQt5 import QtGui as QG
from PyQt5 import QtWidgets as QW


class SaveEventsDialog(QW.QDialog):

    def __init__(self, parent=None):
        super(SaveEventsDialog, self).__init__(parent)
        layout = QW.QVBoxLayout()
        self.setLayout(layout)
        location_layout = QW.QHBoxLayout()
        location_layout.addWidget(QW.QLabel("Saving to:"))
        # try:
        #    self.save_folder_name = Config.get_property('save_folder_name')
        # except KeyError:
        self.save_file_name = None
        self.location_label = QW.QLabel()
        location_layout.addWidget(self.location_label)
        location_change_pb = QW.QPushButton("Choose file")
        location_layout.addWidget(location_change_pb)
        layout.addLayout(location_layout)
        location_change_pb.clicked.connect(self.choose_file)

        button_layout = QW.QHBoxLayout()
        self.save_pb = QW.QPushButton("Save")
        cancel_pb = QW.QPushButton("Cancel")
        button_layout.addWidget(self.save_pb)
        button_layout.addWidget(cancel_pb)
        self.save_pb.clicked.connect(self.accept)
        self.save_pb.setEnabled(False)
        cancel_pb.clicked.connect(self.reject)

        comment_layout = QW.QHBoxLayout()
        comment_layout.addWidget(QW.QLabel("Comment"))
        self.comment_box = QW.QLineEdit()
        # self.comment_box.editingFinished.connect(self.ready)

        comment_layout.addWidget(self.comment_box)
        layout.addLayout(comment_layout)
        layout.addLayout(button_layout)

        self.set_location_label()

    def choose_file(self):
        filename = self.choose_file_dialog()
        self.save_file_name = filename
        self.set_location_label()

    def choose_file_dialog(self):
        save_file_name = str(QW.QFileDialog.getSaveFileName(self)[0])
        return save_file_name

    def set_location_label(self):
        if self.save_file_name:
            if ".csv" not in self.save_file_name:
                self.save_file_name += ".csv"
            self.location_label.setText(self.save_file_name)
            self.save_pb.setEnabled(True)
        else:
            self.location_label.setText("---")
            self.save_pb.setEnabled(False)

    def accept(self):
        self.comment = self.comment_box.text()
        return QW.QDialog.accept(self)
