from analysispanel import AnalysisPanel
from eventpanel import EventPanel
from framepanel import FramePanel
from pipechainpanel import PipeChainPanel
from visualizationpanel import VisualizationPanel
from actionpanel import ActionPanel
