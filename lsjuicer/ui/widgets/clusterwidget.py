from itertools import cycle

import numpy as n

from sklearn.cluster import DBSCAN
import scipy.ndimage as nd
import scipy.stats as ss

from PyQt5 import QtGui as QG
from PyQt5 import QtWidgets as QW

from PyQt5 import QtCore as QC


from lsjuicer.inout.db.sqla import dbmaster
import lsjuicer.inout.db.sqla as sa
import lsjuicer.data.analysis.transient_find as tf
from lsjuicer.ui.widgets.plot_with_axes_widget import TracePlotWidget
from lsjuicer.ui.widgets.fileinfowidget import DBComboAddBox, MyFormLikeLayout, FocusLineEdit
from lsjuicer.util.helpers import timeIt
from lsjuicer.ui.plot.pixmapmaker import nearest_average as nav


def normify(a):
    # you can use preprocessing.scale instead
    # b=10.0
    res = (a - a.mean()) / a.std()
    #left = ss.scoreatpercentile(res,b)
    #right = ss.scoreatpercentile(res,100-b)
    # r2=res.clip(left,right)
    return res


class ClusterWidget(QW.QWidget):

    clusters_ready = QC.pyqtSignal(dict)

    def __init__(self, data, parameter_names, plot_pairs, key, category_class, result,  normalize=False,
                 parent=None):
        """
        Arguments
        ---------
        data : entire data array (possibly containing more columns than needed for clustering)
        parameter_names : the names of the parameters clustering is to be performed for
        plot_pairs : pairs parameter names plots are desired for
        key : dictionary mapping parameter name to its index in data
        result: fitresult
        normalize : whether to normalize data or not

        """
        super(ClusterWidget, self).__init__(parent)
        self.data = data
        self.category_class = category_class
        indices = [key[el] for el in parameter_names]
        self.cluster_data = data[:, indices]
        self.result = result
        self.events = []
        if normalize:
            self.cluster_data = n.apply_along_axis(
                normify, 0, self.cluster_data)
        widget_layout = QW.QVBoxLayout()
        self.plot_layout = QW.QGridLayout()
        self.plot_layout.setContentsMargins(0, 0, 0, 0)
        self.plot_layout.setSpacing(0)
        self.setLayout(widget_layout)
        widget_layout.addLayout(self.plot_layout)
        self.rows = len(plot_pairs.keys())
        #self.columns = max([len(el) for el in plot_pairs.values()])
        self.plotwidgets = {}
        self.plot_pairs = plot_pairs
        self.make_plot_widgets()
        self.key = key
        setting_layout = QW.QHBoxLayout()
        do_pb = QW.QPushButton('Do clustering')
        if self.category_class == sa.EventCategoryShapeType:
            action_pb = QW.QPushButton("Cluster by Location")
            action_pb.clicked.connect(self.emit_ready)
        elif self.category_class == sa.EventCategoryLocationType:
            action_pb = QW.QPushButton("Save clusters")
            action_pb.clicked.connect(lambda: self.save_clusters())
            dilate_pb = QW.QPushButton("Fill gaps")
            dilate_pb.clicked.connect(self.do_dilate)
            setting_layout.addWidget(dilate_pb)
        self.action_pb = action_pb
        self.category_widget = EventCategoryWidget(self.category_class)
        setting_layout.addWidget(self.category_widget)
        setting_layout.addWidget(do_pb)
        setting_layout.addWidget(action_pb)
        do_pb.clicked.connect(self.do)
        widget_layout.addLayout(setting_layout)
        QC.QTimer.singleShot(50, lambda: self.do())

    def do_dilate(self):
        dims = self.result.active_dimensions()
        image_width = dims['width']
        image_height = dims['height']
        max_mu_diff = 20
        pixels = self.result.pixels

        def nanify(m):
            qq = n.where(m > 0, m, n.nan)
            return qq

        for event in self.events:
            # print event
            im = n.zeros((image_height, image_width), dtype='bool')
            times = n.zeros((image_height, image_width), dtype='float')
            for pe in event.pixel_events:
                # print '\t',pe
                im[pe.pixel.y, pe.pixel.x] = True
                times[pe.pixel.y, pe.pixel.x] = pe.parameters['m2']
            dilation_im = nd.binary_dilation(im, iterations=4)
            filled_im = nd.binary_fill_holes(dilation_im)

            new_pes = []
            nan_times = nanify(times)
            tmax = ss.scoreatpercentile(times[times > 0], 99.)
            tmin = ss.scoreatpercentile(times[times > 0], 1.)
            print event, im.sum(), filled_im.sum(), tmax, tmin
            for p in pixels:
                for pe in p.pixel_events:
                    if filled_im[p.y, p.x] and not im[p.y, p.x] and not pe.event:
                        if pe.parameters['m2'] > tmin and pe.parameters['m2'] < tmax:
                            new_pes.append(pe)
                            continue
                        else:
                            ne_av = nav(nan_times, p.x, p.y, maxmargin=5)
                            if abs(pe.parameters['m2'] - ne_av) < max_mu_diff:
                                new_pes.append(pe)
            for pe in new_pes:
                pe.event = event

    def do(self):
        self.action_pb.setEnabled(False)
        eps = self.category_widget.eps
        min_samples = self.category_widget.samples
        self.do_cluster(eps, min_samples)
        self.do_plots()
        self.action_pb.setEnabled(True)

    def do_cluster(self, eps, min_samples):
        print 'do_cluster', eps, min_samples
        self.clusters = Clusterer.cluster_elements(Clusterer.cluster(self.cluster_data,
                                                                     eps=eps, min_samples=min_samples), self.data)
        # print 'clusterkeys',self.clusters.keys()

    def make_plot_widgets(self):
        if self.plotwidgets:
            return
        for i, kind in enumerate(self.plot_pairs.keys()):
            for j, spp in enumerate(self.plot_pairs[kind]):
                x = spp[0]
                y = spp[1]
                plotwidget = TracePlotWidget(self, antialias=False,
                                             xlabel=x, ylabel=y)
                self.plotwidgets[spp] = plotwidget
                if self.rows > 1:
                    self.plot_layout.addWidget(plotwidget, i, j)
                else:
                    self.plot_layout.addWidget(plotwidget, j, 0)
        QW.QApplication.processEvents()

    def do_plots(self):
        colornames = cycle(['red', 'green', 'blue', 'yellow',
                            'orange', 'teal', 'magenta', 'lime', 'navy', 'brown'])
        for spp, plotwidget in self.plotwidgets.iteritems():
            plotwidget.clear()
        for cluster, elements in self.clusters.iteritems():
            group_name = "Group %i" % cluster
            if cluster != -1:
                color = colornames.next()
            else:
                color = 'black'
            style = {'style': 'circles', 'color': color, 'alpha': 0.250}
            if cluster == -1:
                style.update({'size': 0.5, 'alpha': 0.5})
            for i, kind in enumerate(self.plot_pairs.keys()):
                for j, spp in enumerate(self.plot_pairs[kind]):
                    x = self.key[spp[0]]
                    y = self.key[spp[1]]
                    plotwidget = self.plotwidgets[spp]
                    plotwidget.addPlot(group_name, elements[:, x], elements[:, y],
                                       plotstyle=style, hold_update=True)
        for spp, plotwidget in self.plotwidgets.iteritems():
            plotwidget.updatePlots()
            plotwidget.fitView()

    def emit_ready(self):
        self.clusters_ready.emit(self.clusters)

    @timeIt
    def save_clusters(self):
        #make sure settings are saved
        self.category_widget.save()
        self.action_pb.setEnabled(False)
        QW.QApplication.setOverrideCursor(QG.QCursor(QC.Qt.BusyCursor))
        sess = dbmaster.get_session()
        sess.add(self.result)

        def get_pixelevent_by_ids(event_ids):
            result = []
            event_ids = event_ids.astype('int').tolist()
            i = 0
            limit = 999
            # it's necessary to break the queries apart due to sqlite
            # limitations
            while 1:
                imin = limit * i
                imax = min(len(event_ids) - 1, limit * (i + 1))
                ids = event_ids[imin:imax]
                iresult = sess.query(sa.PixelEvent).filter(
                    sa.PixelEvent.id.in_(ids)).all()
                result.extend(iresult)
                i += 1
                if imax == len(event_ids) - 1:
                    break
            return result

        category = self.category_widget.category
        stats = []
        new_events = []
        for event_no in self.clusters:
            if event_no == -1:
                continue
            event = sa.Event()
            sess.add(event)
            new_events.append(event)
            event.category = category
            stats.append(self.clusters[event_no].shape[0])
            event_ids = self.clusters[event_no][:, -1]
            pixelevents = get_pixelevent_by_ids(event_ids)
            for pixel_event in pixelevents:
                event.result = self.result
                pixel_event.event = event
        sess.commit()
        new_event_ids = [event.id for event in new_events]
        if new_event_ids:
            old_events = sess.query(sa.Event).filter(~sa.Event.id.in_(new_event_ids)).\
                join(sa.EventCategory).\
                filter(sa.EventCategory.category_type == category.category_type).\
                filter(sa.Event.result == self.result).all()
            print 'old events', len(old_events), category.category_type.name, new_event_ids
            for old_event in old_events:
                sess.delete(old_event)
        if not new_event_ids:
            save_string = "No events to save"
        else:
            save_string = "<strong>Saved events:</strong><br><br>" + "<br>".join(
                ["{}: {} pixels".format(i + 1, el) for i, el in enumerate(stats)])
        print save_string
        print [len(el.pixel_events) for el in new_events]
        sess.commit()
        self.events = new_events
        self.do_dilate()
        sess.commit()
        print [len(el.pixel_events) for el in new_events]
        self.action_pb.setEnabled(True)
        QW.QApplication.restoreOverrideCursor()
        QW.QMessageBox.information(self, "", save_string)


class Clusterer(object):

    @staticmethod
    def cluster_elements(labels, data):
        groups = {}
        for k in set(labels):
            members = n.argwhere(labels == k).flatten()
            groups[k] = data[members]
        return groups

    @staticmethod
    def cluster(data, eps, min_samples):
        #D = metrics.euclidean_distances(data)
        #S = 1 - (D / n.max(D))
        # print 'clustering', eps, min_samples
        # print data.shape, data[0]
        print "clustering", data.shape, data.mean(axis=0), data.min(axis=0), data.max(axis=0)

        db = DBSCAN(eps=eps, min_samples=min_samples).fit(data)
        #core_samples = db.core_sample_indices_
        labels = db.labels_
        # print set(labels)
        # Number of clusters in labels, ignoring noise if present.
        #n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
        return labels


class ClusterDialog(QW.QDialog):

    def __init__(self, analysis, parent=None):
        super(ClusterDialog, self).__init__(parent)
        layout = QW.QHBoxLayout()
        self.analysis = analysis
        self.setLayout(layout)
        do_pb = QW.QPushButton("Get clusters")
        layout.addWidget(do_pb)
        do_pb.clicked.connect(self.stats)
        self.do_pb = do_pb

    def sizeHint(self):
        return QC.QSize(1300, 1000)

    def stats(self):
        an = self.analysis
        session = dbmaster.get_session()
        # FIXME bad bad
        pixels = an.fitregions[0].results[0].pixels
        skip_x = False
        if an.imagefile.image_frames == 1:
            skip_x = True
        el = tf.do_event_list(pixels, skip_x)

        result = an.fitregions[0].results[0]
        self.result = result
        old_events = session.query(sa.Event).filter(
            sa.Event.result == result).all()
        for event in old_events:
            session.delete(event)
        session.commit()

        # TODO these should not be hardcoded
        shape_params = ['A', 'tau2', 'd2', 'd']
        loc_params = ['m2', 'x', 'y']
        if skip_x:
            loc_params = ['m2', 'y']

        id_param = 'id'
        params = shape_params[:]
        params.extend(loc_params)
        params.append(id_param)
        # dictionary of parameter names and their indices
        ics = dict(zip(params, range(len(params))))
        self.shape_params = shape_params
        self.loc_params = loc_params
        self.ics = ics

        event_array = tf.do_event_array(el, params)
        self.event_array = event_array
        # for shape parameters a normalized array is needed too
        #ea_shape0 = tf.do_event_array(el,shape_params)
        #ea_shape = ea_shape0
        #ea_loc = tf.do_event_array(el,['m2','x','y'])
        tabs = QW.QTabWidget(self)
        self.layout().addWidget(tabs)
        self.tabs = tabs

        shape_plot_pairs = [('d', 'tau2'), ('tau2', 'A'), ('A', 'd')]
        loc_plot_pairs = [('m2', 'x'), ('x', 'y'), ('m2', 'y')]
        if skip_x:
            loc_plot_pairs = [('m2', 'y')]
        self.loc_plot_pairs = loc_plot_pairs
        plot_pairs = {'shape': shape_plot_pairs, 'location': loc_plot_pairs}
        shape_cluster_tab = ClusterWidget(event_array, shape_params,
                                          plot_pairs, ics, sa.EventCategoryShapeType, result,
                                          normalize=True, parent=tabs)
        shape_cluster_tab.clusters_ready.connect(self.add_loc_clusters)
        tabs.addTab(shape_cluster_tab, 'Clusters by shape')
        self.do_pb.setVisible(False)

    def add_loc_clusters(self, cluster_data):
        """Add a tab for each shape cluster to be analyzed based on location"""
        print 'add_loc', cluster_data
        if self.tabs.count() > 1:
            for i in range(1, self.tabs.count())[::-1]:
                w = self.tabs.widget(i)
                w.deleteLater()
                self.tabs.removeTab(i)
        for cluster, elements in cluster_data.iteritems():
            print 'cluster', elements
            if cluster != -1:
                #data = elements[:,[self.ics[e] for e in self.loc_params]]
                #loc_ics = dict(zip(self.loc_params, range(len(self.loc_params))))
                plot_pairs = {'location': self.loc_plot_pairs}
                tab = ClusterWidget(elements, self.loc_params, plot_pairs, self.ics,
                                    sa.EventCategoryLocationType, self.result, parent=self.tabs)
                index = self.tabs.addTab(tab, 'Type %i' % cluster)
            self.tabs.setCurrentIndex(index)


class EventCategoryWidget(QW.QWidget):

    def __init__(self, category_class, parent=None):
        super(EventCategoryWidget, self).__init__(parent)
        self.category_class = category_class
        self.cat_type = None
        self.category = None
        layout = QW.QVBoxLayout()
        self.setLayout(layout)
        gbox = QW.QGroupBox("Clustering settings")
        settings_layout = MyFormLikeLayout()
        gbox.setLayout(settings_layout)
        layout.addWidget(gbox)
        combo = DBComboAddBox(category_class, show_None=False)
        self.combo = combo
        combo.combo.currentIndexChanged[str].connect(self.update_settings)
        settings_layout.add_row("Type", combo)
        settings_combo = QW.QComboBox()
        settings_combo.currentIndexChanged.connect(self.update_spinboxes)
        settings_layout.add_row("Parameters", settings_combo)
        self.settings_combo = settings_combo
        self.settings_combo.setMinimumWidth(250)

        edit_checkbox = QW.QCheckBox(self)
        min_sample_spinbox = QW.QSpinBox(self)
        eps_spinbox = QW.QDoubleSpinBox(self)
        eps_spinbox.setMinimum(0.1)
        eps_spinbox.setMaximum(25.0)
        eps_spinbox.setValue(4.0)
        eps_spinbox.setSingleStep(0.05)
        min_sample_spinbox.setMinimum(2)
        min_sample_spinbox.setMaximum(200)
        min_sample_spinbox.setValue(20)
        min_sample_spinbox.setSingleStep(1)
        desc_edit = FocusLineEdit()
        desc_edit.set_default_text("optional")
        self.desc_edit = desc_edit
        settings_layout.add_row("Edit settings", edit_checkbox)
        settings_layout.add_row("Description", desc_edit)
        settings_layout.add_row("Minimum core samples", min_sample_spinbox)
        settings_layout.add_row("EPS (max distance)", eps_spinbox)
        save_pb = QW.QPushButton("Save settings")
        settings_layout.add_row("", save_pb)

        self.min_sample_spinbox = min_sample_spinbox
        self.eps_spinbox = eps_spinbox
        self.save_pb = save_pb
        edit_checkbox.toggled.connect(self.toggle_edit)
        self.edit_checkbox = edit_checkbox
        self.toggle_edit(False)
        self.save_pb.clicked.connect(self.save)
        self.activate()

    def activate(self):
        name = self.combo.get_value()
        if name:
            self.update_settings(name)
            self.edit_checkbox.setEnabled(True)
        else:
            self.edit_checkbox.setEnabled(False)

    @property
    def eps(self):
        return self.eps_spinbox.value()

    @property
    def samples(self):
        samples = self.min_sample_spinbox.value()
        return samples

    def save(self):
        sess = dbmaster.get_session()
        temp_cat_type = self.category_class()
        samples = self.samples
        name = self.combo.get_value()
        eps = self.eps
        desc = self.desc_edit.get_text()

        # TODO only make new category if the saved one has events associated with it
        #(we dont want to change the settings for used categories)
        make_new = False
        print 'category a', self.category
        if self.category:
            sess.add(self.category)
            if not self.category.events:
                # no events so category can be updated
                print 'category update'
                self.category.eps = eps
                self.category.min_samples = samples
                self.category.description = desc
                self.category_type = self.cat_type
                # self.reload_settings()
                sess.commit()
                self.activate()
            else:
                print 'description update'
                # events exist but only description is different. update
                # description
                print  self.category.eps == eps ,self.category.min_samples == samples,self.category.description != desc,'ds',self.category.description,'dn', desc
                if self.category.eps == eps and self.category.min_samples == samples:
                    if self.category.description != desc:
                        self.category.description = desc
                        self.reload_settings()
                    else:
                        #nothing to save
                        print "not saving"
                        pass
                else:
                    make_new = True
            sess.commit()
            # sess.expunge(self.category)
        else:
            make_new = True

        if make_new:
            # build a query to find matching categories
            query = sess.query(sa.EventCategory).join(sa.EventCategoryType).\
                filter(sa.EventCategoryType.category_type == temp_cat_type.category_type).\
                filter(sa.EventCategoryType.name == name).\
                filter(sa.EventCategory.eps == eps).\
                filter(sa.EventCategory.min_samples == samples)
            # categories which are exactly the same
            existing = query.filter(
                sa.EventCategory.description == desc).first()
            # categories with different description
            different_desc = query.filter(
                sa.EventCategory.description != desc).first()
            # we don't want categories with same settings but different
            # descriptions
            if existing:
                print "settings alread exist", (samples, eps)
                QW.QMessageBox.information(self, "Category exists",
                                           """<p style='font-weight:normal;'>A <strong
                        style='color:navy;'>{}</strong> category with parameters
                        <br><strong style='color:navy'>{}</strong> already exists</p>"""
                                           .format(temp_cat_type.category_type, str(existing)))
            elif different_desc:
                ec = different_desc
                ec.description = desc
                self.category = ec
                sess.commit()
                self.reload_settings()
                # self.activate()
            else:
                ec = sa.EventCategory()
                ec.eps = eps
                ec.min_samples = samples
                ec.description = desc
                ec.category_type = self.cat_type
                self.category = ec
                sess.add(ec)
                sess.commit()
                self.activate()
                self.settings_combo.setCurrentIndex(
                    self.settings_combo.count() - 1)
        print 'category b', self.category
        self.toggle_edit(True)

    def toggle_edit(self, state):
        self.min_sample_spinbox.setEnabled(state)
        self.eps_spinbox.setEnabled(state)
        self.save_pb.setEnabled(state)
        self.desc_edit.setEnabled(state)
        self.edit_checkbox.setChecked(state)

    def get_category_type_by_name(self, name):
        sess = dbmaster.get_session()
        temp = self.category_class()
        cat_type = sess.query(sa.EventCategoryType).\
            filter(sa.EventCategoryType.name == str(name)).\
            filter(
                sa.EventCategoryType.category_type == temp.category_type).one()
        del temp
        return cat_type

    def reload_settings(self):
        name = self.combo.get_value()
        sess = dbmaster.get_session()
        temp = self.category_class()
        self.settings = sess.query(sa.EventCategory).join(sa.EventCategoryType).\
            filter(sa.EventCategoryType.category_type == temp.category_type).\
            filter(sa.EventCategoryType.name == str(name)).all()
        del temp

    def update_settings(self, name):
        """Update categorytype"""
        if not name:
            # DBComboBox updates stuff internally and can remove entries
            # To keep this function from crashing such calls are ignored here
            return
        self.reload_settings()
        self.cat_type = self.get_category_type_by_name(name)
        self.settings_combo.clear()
        if not self.settings:
            self.edit_checkbox.setEnabled(True)
            self.edit_checkbox.setChecked(True)
            self.category = None
        else:
            # print self.settings
            for s in self.settings:
                self.settings_combo.addItem(str(s))
            self.category = self.settings[0]

    def update_spinboxes(self, index):
        """Update category settings"""
        try:
            setting = self.settings[index]
            self.category = setting
            self.min_sample_spinbox.setValue(setting.min_samples)
            self.eps_spinbox.setValue(setting.eps)
            self.desc_edit.set_text(setting.description)
            # self.toggle_edit(False)
            # self.edit_checkbox
        except IndexError:
            self.min_sample_spinbox.setValue(0)
            self.eps_spinbox.setValue(0)
            self.desc_edit.set_text("")
