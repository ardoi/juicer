# Installing

## Windows

1. Download and install [Miniconda](https://conda.io/miniconda.html) package management system (64 bit and Python2.7). This will help install all the necessary scientific computation libraries and dependencies for juicer.

2. If you do not have Java installed on your machine go [here](https://java.com/en/download/), download and install it. Java is used to provide support for handling multiple microscope image file formats.

3. Download and unzip Juicer [code](https://bitbucket.org/ardoi/juicer/get/default.zip).

4. Run **Anaconda2** -> **Anaconda Prompt** from the Windows menu.

5. Run the following command:

    ```
    conda create -n juicer -c conda-forge  python=2.7 scikit-learn=0.19.2 appdirs=1.4.3 pyqt=5.6.0 sqlalchemy=1.2.10 scikit-image=0.14.2 inflect=0.2.5 git
    ```
    
    This will create an environment for Juicer and will install all the packages necessary for running Juicer.
    
6. Activate the newly created environment:

    ```
    conda activate juicer
    ```

7. In the Anaconda prompt navigate to the folder where Juicer is (downloaded in step 3). You can do this using the `cd` command (e.g., `cd C:\code\juicer`). Once in the correct location start Juicer by running:

    ```
    python run_qt.py
    ```
    
8. Download and open the [test image](https://bitbucket.org/ardoi/juicer/src/default/test/Image0022.oib) in Juicer.

## MacOS/Linux

1. Download and install [Miniconda](https://conda.io/miniconda.html) package management system (64 bit and Python2.7). This will help install all the necessary scientific computation libraries and dependencies for juicer.

2. If you do not have Java installed on your machine, either install it via your package manager or go [here](https://java.com/en/download/), download and install it. Java is used to provide support for handling multiple microscope image file formats.

3. Download and unzip Juicer [code](https://bitbucket.org/ardoi/juicer/get/default.zip).

4. Open a Terminal.

5. Run the following command:

    ```
    conda create -n juicer -c conda-forge  python=2.7 scikit-learn=0.19.2 appdirs=1.4.3 pyqt=5.6.0 sqlalchemy=1.2.10 scikit-image=0.14.2 inflect=0.2.5 git
    ```
    
    This will create an environment for Juicer and will install all the packages necessary for running Juicer.

6. Activate the newly created environment:

    ```
    source activate juicer
    ```

7. In the Anaconda prompt navigate to the folder where Juicer is (downloaded in step 3). You can do this using the `cd` command (e.g., `cd ~/code/juicer`). Once in the correct location start Juicer by running:

    ```
    python run_qt.py
    ```
    
8. Download and open the [test image](https://bitbucket.org/ardoi/juicer/src/default/test/Image0022.oib) in Juicer.


