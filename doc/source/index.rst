.. juicer documentation master file, created by
   sphinx-quickstart on Mon Dec 13 17:03:00 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to juicer's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

.. .. automodule:: views
    :members:
.. .. automodule:: widgets
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Modules
=======

.. toctree::
    :maxdepth: 2

    modules/views
    modules/widgets
    modules/analysisresult

