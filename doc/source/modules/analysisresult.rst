:mod:`analysis`
================

.. automodule:: AnalysisResult

.. autoclass:: Result
    :members:

.. .. autosummary::
    AnalysisResult.Result


