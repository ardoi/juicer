:mod:`widgets`
==============

.. .. automodule:: data

.. automodule:: data.datamodels
    :members:
    :show-inheritance:

.. .. autoclass:: PacingAnalysisWidget
    :members:
    :show-inheritance:
